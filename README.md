# `api-placeholder`

[![pipeline status](https://gitlab.com/mycf.sg/api-placeholder/badges/master/pipeline.svg)](https://gitlab.com/mycf.sg/api-placeholder/-/commits/master)


This repository contains a placeholder service used during cluster migrations to ensure configurations are in place before replacing the image.

The Docker image can be found at [https://hub.docker.com/r/mycfsg/api-placeholder](https://hub.docker.com/r/mycfsg/api-placeholder).

# Usage

## Configuration

| Environment Key | Default | Description |
| --- | --- | --- |
| `BIND_ADDR` | `0.0.0.0:1337` | Interface for server to bind to |

## Docker

To pull in the image:

```sh
docker pull mycfsg/api-placeholder:latest;
```

To run a container based on this image:

```sh
docker run -it -p 1337:1337 mycfsg/api-placeholder:latest;
```

## Kubernetes

The following is a Kuberentes manifest that works:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: placeholder
  labels: &labels
    app: placeholder
    type: service
spec:
  replicas: 1
  selector:
    matchLabels:
      <<: *labels
  template:
    metadata:
      labels:
        <<: *labels
    spec:
      containers:
      - name: placeholder
        image: mycfsg/api-placeholder:latest
        ports:
        - containerPort: 1337
        imagePullPolicy: Always
        resources:
          limits:
            cpu: 32m
            memory: 64Mi
          requests:
            cpu: 25m
            memory: 50Mi
---
apiVersion: v1
kind: Service
metadata:
  name: placeholder
  labels:
    app: placeholder
    type: service
spec:
  ports:
  - port: 1337
    targetPort: 1337
  selector:
    app: placeholder
```

- You should be able to access your application from within the cluster at `http://placeholder.default.svc.cluster.local:1337`
- You should be able to run `kubectl get pods | grep placeholder` to get the pod name once the above YAML is applied
- You should be able to run `kubectl exec -it ${POD_NAME} /bin/sh` to gain shell access to the service

# Development Runbook

## Getting Started

1. Clone this repository
2. Run `make deps` to pull in external dependencies
3. Write some awesome stuff
4. Run `make test` to ensure unit tests are passing
5. Run `make build` to ensure the binary is buildable
6. Run `make image` to ensure the binary is production-compilable
7. Push to remote

## Continuous Integration (CI) Pipeline

### On Gitlab

Gitlab is used to run tests and ensure that builds run correctly.

#### Version Bumping

1. Run `make .ssh`
2. Copy the contents of the file generated at `./.ssh/id_rsa.base64` into an environment variable named **`DEPLOY_KEY`** in **Settings > CI/CD > Variables**
3. Navigate to the **Deploy Keys** section of the **Settings > Repository > Deploy Keys** and paste in the contents of the file generated at `./.ssh/id_rsa.pub` with the **Write access allowed** checkbox enabled

- **`DEPLOY_KEY`**: generate this by running `make .ssh` and copying the contents of the file generated at `./.ssh/id_rsa.base64`

#### DockerHub Publishing

1. Login to [https://hub.docker.com](https://hub.docker.com), or if you're using your own private one, log into yours
2. Navigate to [your security settings at the `/settings/security` endpoint](https://hub.docker.com/settings/security)
3. Click on **Create Access Token**, type in a name for the new token, and click on **Create**
4. Copy the generated token that will be displayed on the screen
5. Enter the following varialbes into the CI/CD Variables page at **Settings > CI/CD > Variables** in your Gitlab repository:

- **`DOCKER_REGISTRY_URL`**: The hostname of the Docker registry (defaults to `docker.io` if not specified)
- **`DOCKER_REGISTRY_USERNAME`**: The username you used to login to the Docker registry
- **`DOCKER_REGISTRY_PASSWORD`**: The generated access token

# License

Code in this repository is licensed under the [MIT License](./LICENSE)
