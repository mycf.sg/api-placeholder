package main

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func init() {
	conf.LoadFromEnvironment()

}

func main() {
	logrus.Info("starting application...")
	cmd := &cobra.Command{
		Use: "api-placeholder",
		Run: run,
	}
	cmd.Execute()
}

func run(_ *cobra.Command, _ []string) {
	logrus.Debug("preparing handler...")
	handler := http.NewServeMux()
	handler.HandleFunc("/", handleRequest)

	logrus.Debug("preparing server...")
	server := newServer(handler)

	logrus.Info("starting server...")
	if listenError := server.ListenAndServe(); listenError != nil {
		logrus.Error(listenError)
	}
}
