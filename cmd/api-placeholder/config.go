package main

import "github.com/usvc/go-config"

const (
	ConfigBindAddress = "bind_addr"
)

var conf = config.Map{
	ConfigBindAddress: &config.String{
		Default:   "0.0.0.0:1337",
		Shorthand: "b",
		Usage:     "address to bind to",
	},
}
