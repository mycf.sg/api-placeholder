package main

import (
	"time"

	"github.com/sirupsen/logrus"
)

func init() {
	logrus.SetLevel(logrus.TraceLevel)
	logrus.SetReportCaller(true)
	logrus.SetFormatter(&logrus.JSONFormatter{
		DisableTimestamp: false,
		TimestampFormat:  time.RFC1123,
		DataKey:          "@data",
		FieldMap: logrus.FieldMap{
			logrus.FieldKeyFile:  "@file",
			logrus.FieldKeyFunc:  "@func",
			logrus.FieldKeyLevel: "@level",
			logrus.FieldKeyMsg:   "@msg",
			logrus.FieldKeyTime:  "@timestamp",
		},
	})
	logrus.Info("finished init successfully")
}
