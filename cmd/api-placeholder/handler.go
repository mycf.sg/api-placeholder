package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

func handleRequest(res http.ResponseWriter, req *http.Request) {
	now := time.Now()
	requestID := uuid.New().String()
	timestamp := now.Format(time.RFC3339)
	headers := map[string]interface{}{}
	for key, value := range req.Header {
		headers[key] = value
	}
	cookies := map[string]interface{}{}
	for _, cookie := range req.Cookies() {
		cookies[cookie.Name] = cookie.Raw
	}
	body, readError := ioutil.ReadAll(req.Body)
	if readError != nil {
		body = []byte(readError.Error())
	}
	fields := map[string]interface{}{
		"method":      req.Method,
		"host":        req.Host,
		"remote_addr": req.RemoteAddr,
		"protocol":    req.Proto,
		"hostname":    req.URL.Hostname(),
		"port":        req.URL.Port(),
		"path":        req.URL.Path,
		"queries":     req.URL.RawQuery,
		"cookies":     cookies,
		"headers":     headers,
		"timestamp":   timestamp,
		"body":        string(body),
		"duration_ms": time.Since(now).Microseconds(),
	}
	log := logrus.WithFields(logrus.Fields(fields))
	log.Time = now
	log.Infof("request received")
	responseBody, marshalError := json.Marshal(fields)
	if marshalError != nil {
		log.Errorf("failed to marshal %v", fields)
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(fmt.Sprintf(`{"requestID":"%s","timestamp":"%s","error":"%s"}`, requestID, timestamp, marshalError)))
		return
	}
	res.Write(responseBody)
}
