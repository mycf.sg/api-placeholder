package main

import (
	"net/http"
	"time"
)

func newServer(handler http.Handler) http.Server {
	return http.Server{
		Addr:              conf.GetString(ConfigBindAddress),
		Handler:           handler,
		MaxHeaderBytes:    1024 * 16,
		IdleTimeout:       time.Second * 3,
		ReadHeaderTimeout: time.Second * 3,
		WriteTimeout:      time.Second * 3,
		ReadTimeout:       time.Second * 3,
	}
}
