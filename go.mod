module gitlab.com/mycf.sg/api-placeholder

go 1.14

require (
	github.com/google/uuid v1.1.1
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.0.0
	github.com/usvc/go-config v0.2.2
)
